# 時間が余ったら

もし時間が余ったら、アプリケーションの改良をしてみましょう。

以下にお題を挙げておきます。
もちろん、下に挙がっていない改良でも構いません。

* 画面内のボタンをコンポーネント化して、他の画面でも簡単に利用できるようにしてみましょう。  
