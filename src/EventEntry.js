import React, { useState } from 'react';
import { withRouter } from 'react-router';
import { firebaseApp } from './config/firebase'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import Chip from '@material-ui/core/Chip';
import { TextField } from '@material-ui/core';

import './EventEntry.css';

const firebaseDb = firebaseApp.database();

const EventEntry = (props) => {
    // 1-1.入力フォームの内容をstateに反映しよう

    // イベント候補日
    const [possibleDateText, setPossibleDateText] = useState('')

    // "イベントを作る"ボタンを押すとregisterEvent関数が起動します。
    const registerEvent = () => {
        const possibleDates = possibleDateText.split('\n')
        // 1-3-1.入力した値を整形しよう

        // 1-3-2.Realtime Databaseに整形した値を書き込もう

        // 1-4.イベントIDを取得して画面遷移しよう

        // props.history.push(`/event/${eventId}`);

    };


    return (
        <Grid
            id="event-entry"
            container
            item
            direction="row"
            justify="space-between"
            alignItems="flex-start"
            xs={9}
            >
                <Grid container item xs={5} justify="flex-start">
                    <div className="guide-title">
                        <Chip color="primary" label="1" className="guide-title__chip"/>
                        イベント概要を入力しましょう
                    </div>
                    <TextField
                    placeholder="イベント名"
                    //1-1入力された値をstateで管理しましょう

                    // 1-2.入力フォームの内容をstateから取得して表示しよう

                    fullWidth={true}
                    variant="outlined"
                    />
                    <TextField
                    placeholder="説明"
                    //1-1入力された値をstateで管理しましょう

                    // 1-2.入力フォームの内容をstateから取得して表示しよう

                    margin="normal"
                    multiline
                    rows={7}
                    fullWidth={true}
                    variant="outlined"
                    />
                </Grid>
                <Grid container item xs={5} justify="flex-start">
                    <div className="guide-title">
                        <Chip color="primary" label="2" className="guide-title__chip"/>
                        イベント候補日を入力しましょう
                    </div>
                    <TextField
                    placeholder="例：12/4 18:00~"
                    value={possibleDateText}
                    onChange={evt => setPossibleDateText(evt.target.value)}

                    className="Guide-title"
                    multiline
                    rows={7}
                    fullWidth={true}
                    variant="outlined"
                    />
                </Grid>
                <Grid container item xs={12} justify="flex-end" className="button-area">
                    <Grid item>
                        <Button
                            variant="contained"
                            color="primary"
                            onClick={() => registerEvent()}>
                            イベントを作る
                        </Button>
                    </Grid>
                </Grid>
        </Grid>
    );
}

export default withRouter(EventEntry);
